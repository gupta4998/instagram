import React,{useEffect,createContext,useReducer, useContext} from 'react'
import "./App.css";
import Navbar from './component/Navbar'
import Home from './component/screen/Home'
import SignIn from './component/screen/SignIn'
import Signup from './component/screen/Signup'
import Profile from './component/screen/Profile'
import CreatePost from './component/screen/CreatePost'
import UserProfile from "./component/screen/UserProfile"
import {BrowserRouter,Route,Switch,useHistory} from 'react-router-dom'
import {reducer,initialState} from './component/reducers/userReducer'

export const UserContext=createContext()

const Routing=()=>{
  const history=useHistory()
  const {state,dispatch}= useContext(UserContext)
  useEffect(()=>{
    const user=JSON.parse(localStorage.getItem("user"))
    console.log(user)
    if(user){
      dispatch({type:"USER",payload:user})
    }else{
      history.push('/signin')
    }
  },[])
  return(
    <Switch>
      <Route exact path="/"><Home/></Route>
   <Route path="/signin"><SignIn/></Route>
   <Route exact path="/profile"><Profile/></Route>
   <Route path="/signup"><Signup/></Route>
   <Route path="/create"><CreatePost/></Route>
   <Route path="/profile/:userid"><UserProfile/></Route>
    </Switch>

  )
}

function App() {
  const [state,dispatch]=useReducer(reducer,initialState)
  return (
    <UserContext.Provider value={{state,dispatch}}>
    <BrowserRouter>
   <Navbar/>
   <Routing/>
   </BrowserRouter>
   </UserContext.Provider>
  );
}

export default App;
