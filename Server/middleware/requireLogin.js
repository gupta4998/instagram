const jwt=require('jsonwebtoken')
const {jwt_scecretKey}=require('../key')
const mongoose=require('mongoose')
const User=mongoose.model("User")


const requireLogin=(req,res,next)=>{
    const {authorization}=req.headers
    if(!authorization){
        return res.status(401).json({error:"you must login first"})
    }
    const token=authorization.replace("Bearer ","")
    jwt.verify(token,jwt_scecretKey,(err,payload)=>{
        if(err){
            return res.status(401).json({error:"you must login first"})
        }

        const {_id}=payload
        User.findById(_id).then((userData)=>{
            req.user=userData
            next()
        })
        
    })

}

module.exports=requireLogin