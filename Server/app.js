const express =require('express');
const app=express();
const mongoose=require('mongoose')
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const {db}=require('./key')
require('./models/user')
require('./models/post')
const auth=require('./routes/auth')
const post=require('./routes/post')
const user=require('./routes/user')

app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.json())  //take all incoming request and pass to json



// const costomMiddleWare=(req,res,next)=>{
//     console.log("middleware executed")
//     next()
// }

// //app.use(costomMiddleWare)

// app.get('/',(req,res)=>{
//     console.log('hello')
//     res.send('hello world')
// })
// app.get('/about',costomMiddleWare,(req,res)=>{
//     console.log('about')cc
//     res.send('about page')
// })

mongoose.connect(db,{
    useNewUrlParser:true,
    useUnifiedTopology:true,
    useFindAndModify:false
})
.then(()=>{
    console.log('mongoDb connected..')
})

app.use(auth)
app.use(post)
app.use(user)


app.listen(5000,()=>{
    console.log("server is running",5000)
})