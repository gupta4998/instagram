const express=require('express')
const router=express.Router()
const mongoose=require('mongoose')
const User=mongoose.model('User')
const bcrypt=require('bcryptjs')
const jwt=require('jsonwebtoken')
const {jwt_scecretKey}=require('../key')
const requireLogin=require('../middleware/requireLogin')


router.get('/protected',requireLogin,(req,res)=>{
    res.send("hello abhi")
})


router.post('/signup',(req,res)=>{
    //console.log(req.body)
    const{name,email,password}=req.body
    if(!name||!email||!password){
        return res.status(422).json({error:"all field are mandatory"})
    }
    User.findOne({email:email})
    .then((savedUser)=>{
        if(savedUser){
            return res.status(422).json({error:"user already exists with that email"})
        }
        bcrypt.hash(password,12)
        .then(hashedpassword=>{
            const user= new User({
                email,
                password:hashedpassword,
                name
            })
    
            user.save()
            .then(user=>{
                res.json({messege:"saved successfully"})
            })
            .catch(err=>{
                console.log(err)
            })

        })
        
    })
    .catch(err=>{
        console.log(err)
    })
})

router.post('/signin',(req,res)=>{
    const{email,password}=req.body
    if(!email||!password){
        return res.status(422).json({error:"please enter email and passwor"})
    }
    User.findOne({email:email})
    .then((savedUser)=>{
        if(!savedUser){
            return res.status(422).json({error:"invalid email and password"})
        }
        bcrypt.compare(password,savedUser.password)
        .then((doMatch)=>{
            if(doMatch){
                //return res.json({messege:"successfully login"})
                const token= jwt.sign({_id:savedUser._id},jwt_scecretKey)
                const {_id,name,email}=savedUser
                res.json({token,user:{_id,name,email}})
            }
            else{
                return res.status(422).json({error:"invalid email and password"})
            }
        })
        .catch((err)=>{
            console.log(err)
        })
    })
})

module.exports=router